;; -*- mode: Lisp; Syntax: COMMON-LISP; Base: 10; eval: (hs-hide-all) -*-

(defpackage #:geoduck
  (:use #:cl)
  (:export :widget
           :->
           :pack
           :*handlers*
           :start-ws-server
           :ws-client-lib
           :prepare-html
           :compile-widget
           :pack
   :send-message
   :unique-id
   :broadcast))

(in-package #:geoduck)

(defclass widget ()
  ((name :initarg :name
         :accessor widget-name
         :initform nil)
   (view :initarg :view
         :accessor widget-view
         :initform nil
         :json-type :string
         :json-key "view")
   (control :initarg :control
            :accessor widget-control
            :initform nil
            :json-type :string
            :json-key "control")
   (injector :initarg :injector
             :accessor widget-injector
             :initform nil
             :json-type :string
             :json-key "injector")
   (parent :initarg :parent
           :accessor widget-parent
           :initform "hidden"
           :json-type :string
           :json-key "parent")
   (unique :initarg :unique
           :accessor widget-unique
           :json-type :string
           :json-key "unique")
   (api :initarg :api
        :initform nil
        :accessor widget-api))
  (:metaclass json-mop:json-serializable-class))

(ps:defpsmacro -> (&rest body)
  `(ps:chain ,@body))

(defun unique-id ()
  (string-downcase
   (ppcre:regex-replace-all "-" (format nil "~a" (uuid:make-v4-uuid)) "")))

;; TODO: don't release this without cleaning this shit up.
(defmethod compile-widget ((w widget))
  (let* ((unique (unique-id))
         (good-view (cl-ppcre:regex-replace-all "UNIQUEID" (widget-view w) unique))
         (good-js (cl-ppcre:regex-replace-all "UNIQUEID" (widget-control w) unique)))
         
    (make-instance 'widget
                   :control (or good-js "")
                   :view (or good-view "")
                   :parent (or (widget-parent w) "")
                   :name (widget-name w)
                   :unique unique
                   :injector (ps:ps*
                              `(progn
                                 (-> ($ ,(format nil "#~a" (widget-parent w)))
                                     (append (-> j-query (parse-h-t-m-l ,good-view)))))))))

(defmethod pack ((w geoduck:widget))
  (with-output-to-string (sink)
    (json-mop:encode (compile-widget w) sink)))

(defmacro prepare-html (&rest body)
  `(who:with-html-output-to-string (*standard-output* nil :prologue nil :indent nil)
     (who:htm ,@body)))



(defparameter *ws-listener* nil)
(defparameter *handlers* nil)

(defclass chat-room (hunchensocket:websocket-resource)
  ((name :initarg :name
         :initform (error "Room with no name")
         :reader name))
  (:default-initargs :client-class 'user))

(defclass user (hunchensocket:websocket-client)
  ((name :initarg :user-agent
         :reader user-name
         :initform (error "User without name"))))

(defvar *end-points* (list (make-instance 'chat-room :name "geoduck")))

(defun find-room (request)
  (declare (ignore request))
  ;;(log:info request)
  ;;(find (hunchentoot:script-name request) *end-points* :test #'string= :key #'name)
  (car *end-points*))

(pushnew 'find-room hunchensocket:*websocket-dispatch-table*)

(defmethod hunchensocket:client-connected ((room chat-room) user)
  (log:info "Connected..."))
(defmethod hunchensocket:client-disconnected (client resource)
  (log:info "Disconnected..."))

(defun find-handlers (name)
  (remove-if-not (lambda (widget)
                   (string= (widget-name widget) name))
                 *handlers*))

(defmethod hunchensocket:text-message-received ((room chat-room) user message)
  (log:info message)
  (let ((parsed (ignore-errors (yason:parse message))))
    (when parsed
      (let ((keys (alexandria:hash-table-keys parsed)))
        (mapc (lambda (key)
                (when (string= key "event")
                  (mapcar (lambda (widget)
                            (unless (null (slot-value widget 'api))
                              (let ((result (funcall (widget-api widget)
                                                     (gethash "args" parsed)
                                                     widget))
                                    (event-name (gethash "event" parsed)))
                                (log:info event-name result)))
                            (send-message user (geoduck:pack widget)))
                          (find-handlers (gethash key parsed)))))
              keys)))))

(defun start-ws-server (port)
  (setq *ws-listener* (make-instance 'hunchensocket:websocket-acceptor
                                     :port port
                                     :address "127.0.0.1"))
  (hunchentoot:start *ws-listener*))

(defun broadcast (message &rest args)
  (dolist (user (hunchensocket:clients (car *end-points*)))
    (send-message user (apply #'format nil message args))))

(defun ws-client-lib (url)
  (ps:ps*
   `(progn
      (defvar +console-out+)
      (defvar wsocket)
      (defun send-event (name args)
        (-> wsocket (send (-> -j-s-o-n (stringify (ps:create "event" name
                                                             "args" args))))))
      (defun connect ()
        (setf wsocket (ps:new (-web-socket ,url)))
        (setf (ps:@ wsocket onopen) (lambda (e)
                                      (set-interval (lambda ()
                                                      (ping))
                                                    60000)
                                      (send-event "hello")))
        (setf (ps:@ wsocket onclose) (lambda (e)
                                       (reconnect)))
        
        (setf (ps:@ wsocket onmessage) (lambda (e)
                                         (let ((parsed (-> -j-s-o-n (parse (ps:@ e data)))))
                                           (when (and (> (length (ps:@ parsed parent)) 0)
                                                      (ps:@ parsed injector))
                                             (not-eval (ps:@ parsed injector)))
                                           (when (ps:@ parsed control)
                                             (set-timeout (lambda ()
                                                            (not-eval (ps:@ parsed control)))
                                                          1000)))))
        (setf (ps:@ wsocket onerror) (lambda (e)
                                       (reconnect))))
      
      (defun reconnect ()
        (set-timeout (lambda ()
                       (connect))
                     1000))
      
      (defun ping ()
        (let ((message (-> -j-s-o-n (stringify (ps:create "PING" "FLONG")))))
          (-> wsocket (send message))))
      
      (defun not-eval (string)
        (let ((s (-> document (create-element "script"))))
          (setf (ps:@ s src) (concatenate 'string
                                          "data:text/javascript,"
                                          (encode-u-r-i-component string)))
          (-> document body (append-child s)))))))

(defun send-message (user message)
  (hunchensocket:send-text-message user message))
